<?php

use App\Http\Controllers\Api\ResidueController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/residues', [ResidueController::class, 'index'])->name('residue.index');
Route::get('/residues/{residue}', [ResidueController::class, 'show'])->name('residue.show');
Route::get('/import/process/{job}', [ResidueController::class, 'showImportProcess'])->name('residue.import.process');
Route::post('/residues', [ResidueController::class, 'store'])->name('residue.store');
Route::put('/residues/{residue}', [ResidueController::class, 'update'])->name('residue.update');
Route::delete('/residues/{residue}', [ResidueController::class, 'destroy'])->name('residue.delete');
