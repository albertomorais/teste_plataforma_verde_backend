## Teste plataforma verde Backend

<p>Micro projeto de api</p>

### Para instalar
- Clone o projeto.
- copie o arquivo .env.example para .env  e configure de acordo com sua base.
- Rode o comando `composer install`.
- Rode o comando `php artisan migrate`.
- Rode o comando `php artisan serve`.

<p> Para o funcionamento da fila em outro terminal ou em background</p>

- Rode o comando `php artisan queue:work`.

<p> Para rodar os tests</p>

- copie o arquivo .env.testing.exemple para .env.testing e configure de acordo com sua base de teste.
- Rode o comando `php artisan queue:work --env=testing`
- Rode o comando `php artisan test --env=testing`

### Documentação da API
`https://documenter.getpostman.com/view/2007476/2s8YYLLNVc#745d3e8f-50c2-4f43-9fd9-e7419129dd58`
