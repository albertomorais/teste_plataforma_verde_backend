<?php

namespace App\Jobs;


use App\Models\Residue;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Rap2hpoutre\FastExcel\FastExcel;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class ProcessFileResidues implements ShouldQueue
{
    use Dispatchable, IsMonitored, InteractsWithQueue, Queueable, SerializesModels;

    private $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $collection = (new FastExcel())->import($this->file);
        try {

            foreach ($collection as $key => $item) {
                sleep(1);
                $progress = (($key+1) / $collection->count()) * 100;
                $this->queueProgress(number_format($progress,0));
                $item = array_filter($item);
                Residue::create([
                    'description' => $item['Nome Comum do Resíduo'],
                    'tp_residuo' => $item['Tipo de Resíduo'],
                    'category' => $item['Categoria'],
                    'tp_treatment_technology' => $item['Tecnologia de Tratamento'],
                    'class' => $item['Classe'],
                    'unit_measurement' => $item['Unidade de Medida'],
                    'weight' => $item['Peso']
                ]);
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), [
                'storage_path' => $this->file,
                'exception' => $exception
            ]);
        }
    }

    public function failed($exception)
    {
        dump($exception->getMessage());
    }

}
