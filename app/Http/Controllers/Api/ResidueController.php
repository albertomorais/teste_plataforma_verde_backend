<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreResidueRequest;
use App\Http\Requests\UpdateResidueRequest;
use App\Jobs\ProcessFileResidues;
use App\Models\Residue;
use Illuminate\Bus\Dispatcher;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Rap2hpoutre\FastExcel\FastExcel;
use romanzipp\QueueMonitor\Models\Monitor;
use Whoops\Exception\ErrorException;

class ResidueController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $residues = Residue::paginate(10);
        if ($residues->count() > 0){
            return response()->json($residues);
        }else{
            return response()->json(['message' => 'Ainda não há resíduos cadastrados'],200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreResidueRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreResidueRequest $request)
    {
        try {
            $storagePath = $request->file('file')->store('file');
            $storagePath = storage_path('app/' . $storagePath);
            $id = app(Dispatcher::class)->dispatch(new ProcessFileResidues($storagePath));
            return response()->json([
                'id' => $id,
                'url_process' => env('APP_URL') . '/api/import/process/' . $id,
                'status' => 'Aguarde...',
                'message' => 'Em alguns instantes processaremos a sua lista'
            ], 200);
        } catch (Exception $exception) {
            \Log::error($exception->getMessage());
            return response()->json([$exception->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Residue $residue
     * @return \Illuminate\Http\Response |* Error handled in file Handle.php
     */
    public function show(Residue $residue)
    {
        return response()->json($residue, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Residue $residue
     * @return \Illuminate\Http\Response
     */
    public function edit(Residue $residue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateResidueRequest $request
     * @param \App\Models\Residue $residue
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateResidueRequest $request, Residue $residue)
    {
        try {
            $residue->fill($request->all());
            $residue->save();
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
            return response()->json($exception, 500);
        }
        return response()->json(['message' => 'Resíduo atualizado com sucesso'], 200);
    }

    /**
     * Show data import and status
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showImportProcess(int $id)
    {
        $job = Monitor::where('job_id', '=', "$id")->first();
        if (!empty($job)){
            unset($job->name, $job->queue, $job->id);
            return response()->json($job, 200);
        }else{
            \Log::error('Essa importação é inexistente');
            return response()->json(['message' => 'Essa importação é inexistente'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Residue $residue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Residue $residue)
    {
        try {
            $residue->delete();
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json(['message' => 'Resíduo removido com sucesso'], 200);

    }
}
