<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateResidueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'description' => 'min:3|max:255',
            'tp_residuo' => 'min:3|max:255',
            'category' => 'min:3|max:255',
            'tp_treatment_technology' => 'min:3|max:255',
            'class' => 'min:3|max:255',
            'weight' => 'numeric',
            'unit_measurement' => 'min:2|max:255|in:kg,ton,un',
        ];
    }

    /**
     * Custom name item request
     * @return string[]
     */
    public function attributes()
    {
        return [
            'description' => 'Descrição',
            'tp_residuo' => 'Tipo de resíduo',
            'category' => 'Categoria',
            'tp_treatment_technology' => 'Tipo de tratamento tecnológico',
            'class' => 'Classe',
            'weight' => 'Peso',
            'unit_measurement' => 'Unidade de medida',
        ];
    }
}
