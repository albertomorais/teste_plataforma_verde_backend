<?php

function readCSV($path){
    $path = storage_path('app/'.$path);
    if (!file_exists($path)) {
        throw new RuntimeException('File not exists', 404);
    }

    $data =[];
    if (($open = fopen($path, "r")) !== FALSE) {

        while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
            $data[] = $data;
        }

        fclose($open);
    }
    return $data;
}
