<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Residue extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'tp_residuo',
        'category',
        'tp_treatment_technology',
        'class',
        'weight',
        'unit_measurement'
    ];
}
