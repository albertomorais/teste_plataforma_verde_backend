<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Residue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use romanzipp\QueueMonitor\Models\Monitor;
use Tests\TestCase;

class ResidueTest extends TestCase
{
    /**
     * Test import residues.
     * @test
     * @return void
     */
    public function import_residues_is_correct()
    {
        $file = $this->getUploadableFile(base_path("tests/files_test/planilha_residuos.xlsx"));
        $response = $this->post( '/api/residues', ['file' => $file]);
        $statusCode = $response->getStatusCode();
        $response = json_decode($response->content());
        $this->assertEquals($response->message, 'Em alguns instantes processaremos a sua lista');
        $this->assertEquals($statusCode, 200);
    }

    /**
     * Test List residues.
     * @test
     * @return void
     */
    public function list_residues_valid_fild()
    {
        $response = $this->get('/api/residues');
        $responseData = json_decode($response->getContent());
        $this->assertEquals($responseData->first_page_url, 'http://localhost:8000/api/residues?page=1');
        $response->assertStatus(200);
    }

    /**
     * Update a Residue.
     * @test
     * @return void
     */
    public function update_residues_valid_message()
    {
        $residue = Residue::orderBy('id', 'desc')->first();
        $response = $this->put('/api/residues/'.$residue->id, ['description' => 'teste']);
        $response->assertJson(['message' => 'Resíduo atualizado com sucesso']);
        $response->assertStatus(200);
    }

    /**
     * Test show residues.
     *
     * @test
     * @return void
     */
    public function show_residues_valid_field()
    {
        $residue = Residue::orderBy('id', 'desc')->first();
        $response = $this->get('/api/residues/'.$residue->id);
        $response->assertJson(['description' => 'teste']);
        $response->assertStatus(200);
    }

    /**
     * Test show residues.
     *
     * @test
     * @return void
     */
    public function show_import_process_residues_valid_job_and_progress()
    {
        $monitor  = Monitor::ordered('id')->first();
        $response = $this->get('/api/import/process/'.$monitor->job_id);
        $response->assertJson(['job_id' => $monitor->job_id]);
        $response->assertJson(['progress' => 100]);
        $response->assertStatus(200);
    }

    /**
     * Delete a Residue.
     * @test
     * @return void
     */
    public function delete_residues_valid_message()
    {
        $residue = Residue::orderBy('id', 'desc')->first();
        $response = $this->delete('/api/residues/'.$residue->id);
        $response->assertJson(['message' => 'Resíduo removido com sucesso']);
        $response->assertStatus(200);
    }


    /**
     * Return UploadedFile to send on Post
     *
     * @param $file
     * @return UploadedFile
     */
    private function getUploadableFile($file)
    {
        $dummy = file_get_contents($file);

        file_put_contents(base_path("tests/" . basename($file)), $dummy);

        $path = base_path("tests/" . basename($file));
        $original_name = 'planilha_residuos.xlsx';
        $mime_type = 'text/xlsx';
        $error = null;
        $test = true;

        $file = new UploadedFile($path, $original_name, $mime_type, $error, $test);

        return $file;
    }

}
